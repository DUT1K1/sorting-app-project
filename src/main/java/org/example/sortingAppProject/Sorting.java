package org.example.sortingAppProject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

/**
 * This class sorts array of integers of up to 10 in quantity
 */
public class Sorting {
    private static final Logger logger = LogManager.getLogger(Sorting.class);

    /**
     * In this main method it doesn't have a return type,
     * so we have to print everything
     */
    public static void main(String[] args) {
        logger.info("the program is starting");
        if (args.length > 10) {
            logger.warn("this may throw exception");
            System.err.println("Error: up to 10 arguments allowed");
            throw new IllegalArgumentException();
        }
        int ARGSLENGTH = args.length;
        if (ARGSLENGTH == 0) {
            logger.info("checking if length is 0");
            System.out.println("There was no input");
            return;
        }
        int[] numbersToBeSorted = new int[ARGSLENGTH];
        for (int i = 0; i < ARGSLENGTH; i++) {
            try {
                numbersToBeSorted[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException numberFormatException) {
                System.out.println("your input was not integer");
                return;
            }
        }

        Arrays.sort(numbersToBeSorted);

        logger.info("printing array");
        for (int number : numbersToBeSorted) {
            System.out.println(number);
        }

        logger.info("this is a final message");
    }
}