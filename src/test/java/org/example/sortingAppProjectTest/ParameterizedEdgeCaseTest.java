package org.example.sortingAppProjectTest;

import org.example.sortingAppProject.Sorting;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class ParameterizedEdgeCaseTest {
    private final String[] input;

    public ParameterizedEdgeCaseTest(String[] input) {
        this.input = input;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> testData() {
        return Arrays.asList(new Object[][]{
                {new String[]{"-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12", "-200"}},
                {new String[]{"12", "-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12", "-200"}},
                {new String[]{"-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12", "-200", "0", "-7", "4", "15", "12", "-200"}},
                {new String[]{"-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12", "-200", "-1", "8", "1", "-1", "22", "0", "-7", "4", "15", "12", "-200"}}
        });
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptions() {
        Sorting sorting = new Sorting();
        sorting.main(input);
    }
}
