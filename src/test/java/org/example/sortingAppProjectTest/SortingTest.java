package org.example.sortingAppProjectTest;

import org.example.sortingAppProject.Sorting;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertEquals;

/**
 * This class tests the correctness of the Sorting class
 * it does so by testing one normal case and three cases
 * which should print out error message.
 */

public class SortingTest {
    /**
     * Since main method doesn't have a return type
     * we should change the output stream from printing to command line
     * to transferring everything to a string
     */
    private ByteArrayOutputStream outputStream;
    private ByteArrayOutputStream errorStream;

    @Before
    public void setUpStreams() {
        outputStream = new ByteArrayOutputStream();
        System.setOut(new PrintStream(outputStream));

        errorStream = new ByteArrayOutputStream();
        System.setErr(new PrintStream(errorStream));
    }

    @Test
    public void testNoInput() {
        String[] args = {};

        Sorting.main(args);

        String expectedOutput = "There was no input\n";
        assertEquals(expectedOutput, outputStream.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testMoreThanTenInput() {
        String[] args = {"5", "7", "2", "14", "1", "52", "11", "42", "111", "-10", "5", "2", "2", "4", "1"};

        Sorting.main(args);

        String expectedOutput = "Error: up to 10 arguments allowed\n";
        assertEquals(expectedOutput, errorStream.toString());
    }

    @Test
    public void testingInvalidInput() {
        String[] args = {"5", "3", "abc"};

        Sorting.main(args);

        String expectedOutput = "your input was not integer\n";
        assertEquals(expectedOutput, outputStream.toString());
    }

    @Test
    public void testingSortingWithNormalInputs() {
        // Arrange
        String[] args = {"5", "3", "7"};

        // Act
        Sorting.main(args);

        // Assert
        String expectedOutput = "3\n5\n7\n";
        Assert.assertEquals(expectedOutput, outputStream.toString());
    }
}
